#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import os
import tempfile

try:
    import yaml
except ImportError:
    logging.error("Cannot import yaml. Please install pyyaml.")
    raise

from vts.runners.host import utils
from vts.utils.python.common import cmd_utils
from host_controller.build import build_flasher


def FindBinary(binary):
    """Finds a binary in PATH.

    Args:
        binary: A string, the name of the binary file.

    Returns:
        A string, the path to the binary.
        None if not found.
    """
    stdout, stderr, return_code = cmd_utils.ExecuteOneShellCommand(
        "which " + binary)
    if return_code == 0:
        return stdout.strip()
    else:
        return None


class MtkFlasher(build_flasher.BuildFlasher):
    """The class that executes MTK flash tool."""
    _FLASH_TOOL_FILE_NAME = "flash_tool"
    _SCATTER_FILE_SUFFIX = "scatter.txt"

    def _ReplaceImagesInScatter(self, old_path, new_path, images):
        """Creates a new scatter file based on an old scatter file and images.

        This method iterates each partition in the input file. If a partition's
        name is in the image dictionary, its image is replaced with the one
        specified in the dictionary.

        Args:
            old_path: A string, the path to the input file.
            new_path: A string, the path to the output file.
            images: A dict where the keys are partition names and the values
                    are paths to images.
        """
        remain_partitions = set(images.iterkeys())
        old_dirname = os.path.dirname(old_path)
        new_dirname = os.path.dirname(new_path)

        with open(old_path, "r") as old_file:
            scatter = yaml.load(old_file)

        for obj in scatter:
            if "partition_name" not in obj or "file_name" not in obj:
                continue
            partition_name = obj["partition_name"]
            if partition_name in images:
                new_file_name = images[partition_name]
                if partition_name != "system":
                    logging.warning("Set %s partition to %s",
                                    partition_name, new_file_name)
                if partition_name in remain_partitions:
                    remain_partitions.remove(partition_name)
                else:
                    logging.warning("More than one %s in %s",
                                    partition_name, old_path)
            else:
                new_file_name = os.path.join(old_dirname, obj["file_name"])
            # MTK flash tool accepts only relative paths starting from scatter
            # file's directory.
            obj["file_name"] = os.path.relpath(new_file_name, new_dirname)

        if remain_partitions:
            logging.warning("Partitions not found in %s: %s",
                            old_path, ", ".join(remain_partitions))

        with open(new_path, "w") as new_file:
            yaml.dump(scatter, new_file, default_flow_style=False)

    def _ExecuteFlashTool(self, bin_path, scatter_path, mode):
        """Starts flash tool and reboots device.

        Args:
            bin_path: The path to the tool.
            scatter_path: The path to the scatter file.
            mode: A string, the flashing mode.

        Returns:
            An integer, the return code of the tool.
        """
        bin_dir = os.path.dirname(bin_path)
        cmd = ("LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s %s -s %s -c %s" %
               (bin_dir, bin_path, scatter_path, mode))
        logging.info(cmd)
        proc = utils.start_standing_subprocess(cmd, check_health_delay=1)

        logging.info("MTK flash tool started. Reboot device.")
        self.device.reboot(restart_services=False)

        if proc.poll() is None:
            logging.error("MTK flash tool does not terminate after reboot.")
            utils.kill_process_group(proc)

        stdout, stderr = proc.communicate()
        logging.info("MTK flash tool stdout:\n%s\nstderr:\n%s", stdout, stderr)
        return proc.returncode

    def Flash(self, images, additional_files, mode):
        """Executes the command of MTK flash tool.

        Args:
            images: A dict containing partitions and paths to device
                           images. This argument is unused in this method.
            additional_files: A dict where the keys are relative paths and the
                              values are full paths to fetched files. It has to
                              contain the scatter file.
            mode: A string. Supported flashing modes are "format", "download",
                  "format-download", and "firmware-upgrade".

        Returns:
            True if successful; False otherwise
        """
        try:
            bin_path = next(x for x in additional_files.itervalues() if
                            os.path.basename(x) == self._FLASH_TOOL_FILE_NAME)
        except StopIteration:
            bin_path = FindBinary(self._FLASH_TOOL_FILE_NAME)

        if not bin_path:
            logging.error("Cannot find binary %s", self._FLASH_TOOL_FILE_NAME)
            return False

        try:
            scatter_path = next(x for x in additional_files.itervalues() if
                                x.endswith(self._SCATTER_FILE_SUFFIX))
        except StopIteration:
            logging.error("Cannot find scatter file.")
            return False

        with tempfile.NamedTemporaryFile(
                suffix=self._SCATTER_FILE_SUFFIX,
                dir=os.path.dirname(scatter_path),
                delete=False) as new_scatter:
            new_scatter_path = new_scatter.name

        try:
            self._ReplaceImagesInScatter(
                scatter_path, new_scatter_path, images)
            new_scatter.close()
            ret_code = self._ExecuteFlashTool(bin_path, new_scatter_path, mode)
        finally:
            os.remove(new_scatter_path)

        return ret_code == 0
