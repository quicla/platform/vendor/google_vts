"""A VTS lab script file for Sony (oc)."""

def EmitConsoleCommands():
    return [
        "fetch --type=pab --branch=git_oc-treble-dev --target=aosp_arm64_a-userdebug --artifact_name=aosp_arm64_a-img-{build_id}.zip --build_id=latest",
        "flash --current",
        "fetch --type=pab --branch=git_oc-dev --target=test_suites_arm64_fastbuild3d_linux --artifact_name=android-vts.zip --build_id=latest --account_id=541462473",
        "test vts-vndk",
    ]
