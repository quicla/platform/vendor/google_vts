"""A VTS lab script file for MTK Gobo BSP (OC-MR1)."""

def EmitConsoleCommands():
    return [
        "fetch --type=ab --branch=git_oc-mr1-dev --target=test_suites_arm64_fastbuild3l_linux --artifact_name=android-vts.zip --build_id=latest",
        "fetch --type=gcs --path=gs://vtslab-mtk/latest/",
        "fetch --type=ab --branch=git_oc-mr1-treble-dev --target=aosp_arm_a-userdebug --artifact_name=aosp_arm_a-img-{build_id}.zip --build_id=latest",
        "flash --flasher_type=host_controller.build.mtk_flasher.MtkFlasher --current system=system.img -- firmware-upgrade",
        "test --keep-result -- vts --shards 2",
        "upload --src={result_zip} --dest=gs://vts-report/{branch}/{suite_plan}/{build_id}/",
    ]
