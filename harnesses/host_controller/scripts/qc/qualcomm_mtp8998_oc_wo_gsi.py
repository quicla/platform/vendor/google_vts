"""A VTS lab script file for Qualcomm MTP8998."""

def EmitConsoleCommands():
    return [
        # not yet supported
        # "fetch clear",  # deletes any previously fetched artifacts from the cache.
        "fetch --type=gcs --path=gs://vtslab-qc/latest",
        "flash --current",
        "fetch --type=pab --branch=git_oc-vts-release --target=vts_arm_64 --artifact_name=android-vts.zip",
        "test vts-vndk",
    ]
