"""A VTS lab script file for Galaxy S7 (oc)."""

def EmitConsoleCommands():
    return [
        "fetch --type=gcs --path=vtslab-slsi/tools/odin/",
        "fetch --type=gcs --path=vtslab-slsi --build_id=latest",
        "fetch --type=pab --branch=git_oc-treble-dev --target=aosp_arm64_a-userdebug --artifact_name=aosp_arm64_a-img-{build_id}.zip --build_id=latest",
        "gsispl --version_from_path=boot.img",
        "flash --flasher_type=custom --flasher_path=odin4 --reboot_mode=download --repackage=tar.md5 -- -a",
        "fetch --type=pab --branch=git_oc-dev --target=test_suites_arm64_fastbuild3d_linux --artifact_name=android-vts.zip --build_id=latest --account_id=541462473",
        "test vts-vndk",
    ]
