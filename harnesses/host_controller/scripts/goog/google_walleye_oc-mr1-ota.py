"""A VTS lab script file for Pixel 2 (oc-mr1 + pi-release gsi)."""

def EmitConsoleCommands():
    return [
        "fetch --type=ab --branch=git_oc-mr1-release --target=walleye-user --artifact_name=walleye-img-{build_id}.zip --build_id=latest",
        "fetch --type=ab --branch=git_pi-release --target=aosp_arm64_ab-userdebug --artifact_name=aosp_arm64_ab-img-{build_id}.zip --build_id=latest",
        "flash --current",
        "fetch --type=ab --branch=git_oc-mr1-release --target=test_suites_arm64 --artifact_name=android-vts.zip --build_id=latest",
        # or fetch pi-release's vts
        # "fetch --type=ab --branch=git_pi-release --target=test_suites_arm64 --artifact_name=android-vts.zip --build_id=latest",
        "copy /google/data/rw/teams/android-vts/vtslab/google-tradefed-vts-prebuilt.jar {vts_tf_home}",
        "test vts-vndk",
    ]
