Release Date: 2017/08/04
Device build ID: 4243705 (branch: oc-release)
VTS build ID: 4244416 (branch: oc-vts-release)
AOSP system build ID: 4237385 (branch: oc-treble-dev)

Total test modules: 96 of 98
Total passed test cases: 4476
Total failed test cases: 1
Total test execution time: 3h 43m 50s
Release decision: ABANDON
Release notes:

Failing tests: 
VtsHalWifiV1_0Target(1 failing test cases)
Known flaky test, bug filed (b/64366038)

Fixed issues:
VtsHalMediaOmxV1_0Host(1 failing test cases)
Known flaky test, fixed on master (b/62602083)

VTS changes contained: 21

Subject
Project
SHA
Author
Committer

syscalls/times03: Cleanups & rewrite.
platform/external/ltp
e46110a
Cyril Hrubis
Steve Muckle

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
c901276
Ruchi Kandoi
Ruchi Kandoi

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
029633d
Ruchi Kandoi
Ruchi Kandoi

Add EMPTY_RECORD for getDeviceIdentity
platform/hardware/interfaces
67901bf
sqian
sqian

NO PARTIAL RERUN Camera: Update stream consumer usage flag
platform/hardware/interfaces
d9a7d3c
Emilian Peev
Emilian Peev

Camera: fix various VTS issues
platform/hardware/interfaces
a0dd95d
Yin-Chia Yeh
Yin-Chia Yeh

Camera: reset callback after test done
platform/hardware/interfaces
719d3fc
Yin-Chia Yeh
Yin-Chia Yeh

Add corresponding error code to fix vts test
platform/hardware/interfaces
1e5b695
sqian
sqian

Audio policy config xsd: add missing gains and address in ports
platform/hardware/interfaces
488beb2
Kevin Rocard
Kevin Rocard

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
15192a9
Badhri Jagan Sridharan
Keun Soo Yim

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
aef4858
Badhri Jagan Sridharan
Keun Soo Yim

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
43ac449
Zhuoyao Zhang
Zhuoyao Zhang

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
8aac1fc
Zhuoyao Zhang
Zhuoyao Zhang

LTP include filter should overwrite staging and disabled rules
platform/test/vts-testcase/kernel
8661e2c
Yuexi Ma
Yuexi Ma

require LTP timer test suite to run in single thread
platform/test/vts-testcase/kernel
179f3aa
Yuexi Ma
Yuexi Ma

LTP runner checks low ram property to choose scenario groups
platform/test/vts-testcase/kernel
4e5056e
Yuexi Ma
Yuexi Ma

LTP include filter should overwrite staging and disabled rules
platform/test/vts-testcase/kernel
88a6be2
Yuexi Ma
Yuexi Ma

require LTP timer test suite to run in single thread
platform/test/vts-testcase/kernel
f673e71
Yuexi Ma
Yuexi Ma

LTP runner checks low ram property to choose scenario groups
platform/test/vts-testcase/kernel
a8f8d2d
Yuexi Ma
Yuexi Ma

LTP number of threads configurable from runner
platform/test/vts-testcase/kernel
ab26517
Yuexi Ma
Yuexi Ma

DO NOT MERGE run everything in single thread mode
platform/test/vts-testcase/kernel
10432ad
Yuexi Ma
Yuexi Ma

