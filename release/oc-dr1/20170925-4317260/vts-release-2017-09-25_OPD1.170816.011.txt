Release Date: 2017/09/25
Device build ID: 4317260 (branch: oc-dr1-release)
VTS build ID: 4317260 (branch: oc-dr1-release)
AOSP system ID: 4317260 (branch: oc-dr1-release)

Total test modules: 138 of 138
Total passed test cases: 6595
Total failed test cases: 23
Total test execution time: 5H 45s
Release decision: DISTRIBUTE

Release notes:
Failing tests: 

VtsHalCameraProviderV2_4Target failure
b/65611319 (known issue, b/64430483 - fixed in wall DVT and Taimen EVT2)

VtsHalOemLockV1_0Target test failure
b/64835403 (known issue)

VtsHalBiometricsFingerprintV2_1TargetReplay
b/63875996 (known issue, merged mr1-dev)

VtsHalGatekeeperV1_0Target
b/63130498  (known issue, verified walleye PVT by Yim)


Fixed issues:

VTS changes contained: 


