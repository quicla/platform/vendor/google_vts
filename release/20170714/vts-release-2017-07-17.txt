release Date: 2017/07/14
VTS build ID: 4183671 (branch: oc-dev)
AOSP system build ID: 4182021 (branch: oc-treble-dev)
Device build ID: 4183671 (branch: oc-dev)
Branch: oc-dev
Total test modules: 98 of 98
Total passed test cases: 6129
Total failed test cases: 4
Total test execution time: 4H 9m 2s
Release decision: DISTRIBUTE
Release notes:

Failing tests:

VtsHalMediaOmxV1_0Host (3 failing test cases)
Known flaky test, fixed on master (b/62602083)

VtsHalWifiV1_0Target (1 failing test case)
Known issue (b/63131342)

Fixed issues:

VtsKernelLtp
VtsTreblePlatformVersionTest

VTS changes contained:

Subject
Project
SHA
Author
Committer

Pavlin Radoslavov
Fix a bug in getprop return value check
platform/test/vts-testcase/hal
11e4bc7
Keun Soo Yim
Keun Soo Yim

Enable 64-bit camera VTS
platform/test/vts-testcase/hal
37efcc8
Emilian Peev
Keun Soo Yim

Add a seed to LTP runner before shuffle
platform/test/vts-testcase/kernel
3413125
Yuexi Ma
Yuexi Ma

VtsKernelConfig: sync Android config fragments
platform/test/vts-testcase/kernel
147a5e0
Steve Muckle
Steve Muckle

ltp: move epoll_ctl01, epoll_wait01 to staging
platform/test/vts-testcase/kernel
d826f6a
Steve Muckle
Steve Muckle

Add a seed to LTP runner before shuffle
platform/test/vts-testcase/kernel
c1f25d5
Yuexi Ma

