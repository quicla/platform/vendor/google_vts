Release Date: 2017/10/26
Device build ID: 4417144 (branch: oc-mr1-release)
VTS build ID: 4417766 (branch: oc-mr1-dev)
AOSP system build ID: 4417144 (branch: oc-mr1-release)

Total test modules: 150 of 150
Total passed test cases: 6770
Total failed test cases: 6
Total test execution time: 4h 27m 40s
Release decision: DISTRIBUTE
Release notes:

Failing tests:

VtsHalOemHookV1_0Host
b/64009226 (known issue)

VtsTreblePlatformVersionTest
b/68296627

VtsKernelLtp
b/68117785 (known issue)

Fixed issues:
VTS changes contained:

Subject
Project
SHA
Author
Committer

