Release Date: 2017/09/26
Device build ID: 4361900 (branch: oc-dr4-release)
VTS build ID: 4361900 (branch: oc-dr4-release)
AOSP system ID: 4361900 (branch: oc-dr4-release)
Build Tag: OPD4.170816.010

Total test modules: 138 of 138
Total passed test cases: 6612
Total failed test cases: 7
Total test execution time: 4h 21m 9s
Release decision: DISTRIBUTE

Failing tests:

VtsHalBiometricsFingerprintV2_1TargetReplay failure (1)
b/63875996 (known issue, merged mr1-dev)

VtsHalOemLockV1_0Target failures (4)
b/64835403 (known issue)

VtsHalWifiV1_0Host failures (2)
b/64611487 (known issue, merged mr1-dev)
