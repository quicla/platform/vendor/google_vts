Release Date: 2017/11/02
Device build ID: 4283328 (branch: oc-r6-release)
VTS build ID: 4412621 (branch: oc-vts-release)
AOSP system build ID: 4412360 (branch: oc-treble-release)

Total test modules: 98 of 98
Total passed test cases: 5297
Total failed test cases: 1
Total test execution time: 2h 9m 37s
Release decision: DISTRIBUTE
Release notes:

Failing tests:

VtsHalMediaOmxV1_0Host(1 failing test cases)
Known flaky test, fixed on oc-dr1 (b/67829980)

Fixed issues:
VTS changes contained:

Subject
Project
SHA
Author
Committer

