Release Date: 2017/10/05
Device build ID: 4376136 (branch: oc-mr1-release)
VTS build ID: 4376136 (branch: oc-mr1-release)
AOSP system ID: 4376136 (branch: oc-mr1-release)
Build Tag: OPM1.171004.001

Total test modules: 148 of 148
Total passed test cases: 6319
Total failed test cases: 766
Total test execution time: 4H 53m 18s
Release decision: Distribute

Release notes:
Failing tests:

VtsHalCameraProviderV2_4Target
b/67028051 (known issue, test code fixed will apply next latest build)

VtsHalDrmV1_0Target
b/66327050 (known issue, test code fixed will apply next latest build)

VtsHalDumpstateV1_0Target
b/66328355 (known issue)

VtsHalGatekeeperV1_0Target
b/63130498 (oc-dr1 verified walleye PVT)

VtsHalMediaOmxV1_0Host
b/67439702

VtsHalNeuralnetworksV1_0Target
b/64316595 (known issue)

VtsHalOemHookV1_0Host
b/34344851 (known issue)

VtsHalOemLockV1_0Target failure
b/65270046

VtsHalRadioV1_0Target
b/65470015 (filed QC b/65838909)

VtsHalTetherOffloadControlV1_0Target
b/67439856

VtsKernelLtp
b/66336091

VtsTrebleVintfTest
b/64316595

VtsHalWifiSupplicantV1_0Target
b/66334487

VtsHalWifiSupplicantV1_0Target
b/66334487 (known issue, test code fixed will apply next latest build)

VtsHalWifiV1_0Host
b/67028766 (known issue, test code fixed will apply next latest build)



Fixed issues:

VtsHalSensorsV1_0Target
b/67439052

KernelProcFileApiTest failure
b/66326278

VtsKernelLibcutilsTest
b/67028906

VtsHalWifiNanV1_0Target
b/66334487


VTS changes contained: 65

Add low memory device version of sched scenario group
platform/external/ltp
16004d5
Yuexi Ma
Yuexi Ma

syscalls/times03: Cleanups & rewrite.
platform/external/ltp
e46110a
Cyril Hrubis
Steve Muckle

Allow partially dynamic profile in the audio policy config
platform/hardware/interfaces
43d25f2
Kevin Rocard
Kevin Rocard

Audio VTS: Look for Audio policy config in all supported folders
platform/hardware/interfaces
6dcc713
Kevin Rocard
Kevin Rocard

bug fix: close file pointer
platform/hardware/interfaces
727b2ba
Ram Mohan M
Pawin Vongmasa

bug fix: restore support for broken flag
platform/hardware/interfaces
dc6270e
Ram Mohan M
Pawin Vongmasa

Add EMPTY_RECORD for getDeviceIdentity
platform/hardware/interfaces
67901bf
sqian
sqian

bug fix: output colorformat configuration fixed
platform/hardware/interfaces
1441864
Ram Mohan M
Pawin Vongmasa

bug fix: handle OMX_EventBufferFlag events
platform/hardware/interfaces
c8df894
Ram Mohan M
Pawin Vongmasa

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
c901276
Ruchi Kandoi
Ruchi Kandoi

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
029633d
Ruchi Kandoi
Ruchi Kandoi

Audio policy config xsd: add missing gains and address in ports
platform/hardware/interfaces
488beb2
Kevin Rocard
Kevin Rocard

Add corresponding error code to fix vts test
platform/hardware/interfaces
1e5b695
sqian
sqian

Update broadcastradio HAL 1.0 VTS tests.
platform/hardware/interfaces
9e7774c
Tomasz Wasilczyk
Tomasz Wasilczyk

Skip direct report test if sensor is not available
platform/hardware/interfaces
31e5bf8
Peng Xu
Peng Xu

Camera: Switch 'cancelPictureFail' to 'cancelPictureNOP'
platform/hardware/interfaces
be95969
Emilian Peev
Emilian Peev

Allow partially dynamic profile in the audio policy config
platform/hardware/interfaces
3092b31
Kevin Rocard
Kevin Rocard

Camera: reset callback after test done
platform/hardware/interfaces
719d3fc
Yin-Chia Yeh
Yin-Chia Yeh

Camera: fix various VTS issues
platform/hardware/interfaces
a0dd95d
Yin-Chia Yeh
Yin-Chia Yeh

graphics: ignore/reduce spurious vsync in VTS
platform/hardware/interfaces
e0ff3e4
Chia-I Wu
Chia-I Wu

Sensor VTS testBatchingOperation flakiness fix
platform/hardware/interfaces
f75f596
Peng Xu
Peng Xu

bug fix: disable timestamp deviation test for audio decoder
platform/hardware/interfaces
eb9e25a
Ram Mohan M
Pawin Vongmasa

bug fix: handle multiple port settings change events signalled at once
platform/hardware/interfaces
a230bad
Ram Mohan M
Pawin Vongmasa

Audio VTS: remove expectation of policy configuration unicity
platform/hardware/interfaces
76efe01
Kevin Rocard
Kevin Rocard

graphics: ignore/reduce spurious vsync in VTS
platform/hardware/interfaces
83b64ff
Chia-I Wu
Chia-I Wu

Allow clearkey tests to run if no vendor modules
platform/hardware/interfaces
5db2e67
Jeff Tinker
Jeff Tinker

Don't send more than 2K to addRngEntropy
platform/hardware/interfaces
3e1267e
Shawn Willden
Shawn Willden

Reduce max keymaster message size to 2K
platform/hardware/interfaces
703c242
Shawn Willden
Shawn Willden

Don't send more than 2K to addRngEntropy
platform/hardware/interfaces
02ffb2b
Shawn Willden
Shawn Willden

bug fix: configure input port buffer size
platform/hardware/interfaces
9ce313a
Ram Mohan M
Pawin Vongmasa

NO PARTIAL RERUN Camera: Update stream consumer usage flag
platform/hardware/interfaces
d9a7d3c
Emilian Peev
Emilian Peev

Audio VTS: do not test duplicate policy configuration file
platform/hardware/interfaces
ff7dcd7
Kevin Rocard
Kevin Rocard

disable RadioHidlTest.requestShutdown
platform/test/vts-testcase/hal
b76f591
Yuexi Ma
Yuexi Ma

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
43ac449
Zhuoyao Zhang
Zhuoyao Zhang

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
8aac1fc
Zhuoyao Zhang
Zhuoyao Zhang

DO NOT MERGE: Revert "Add VNDK version check test case"
platform/test/vts-testcase/hal
cd54cff
Jiyong Park
Jiyong Park

Update runtime-hint for hal tests.
platform/test/vts-testcase/hal
1599814
Zhuoyao Zhang
Zhuoyao Zhang

Increase VTS audio test timeout
platform/test/vts-testcase/hal
40e61f8
Kevin Rocard
Kevin Rocard

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
15192a9
Badhri Jagan Sridharan
Keun Soo Yim

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
aef4858
Badhri Jagan Sridharan
Keun Soo Yim

Fix Qtaguid sock parsing.
platform/test/vts-testcase/kernel
534f46e
Jerry Zhang
Jerry Zhang

fix ltp json config path
platform/test/vts-testcase/kernel
6754fa9
Yuexi Ma
Yuexi Ma

fix ltp json config path
platform/test/vts-testcase/kernel
195643d
Yuexi Ma
Yuexi Ma

LTP number of threads configurable from runner
platform/test/vts-testcase/kernel
ab26517
Yuexi Ma
Yuexi Ma

DO NOT MERGE run everything in single thread mode
platform/test/vts-testcase/kernel
10432ad
Yuexi Ma
Yuexi Ma

remove requirement for CONFIG_SYNC in 4.9
platform/test/vts-testcase/kernel
e8dc18a
Steve Muckle
Steve Muckle

Update runtime-hint for kernel tests.
platform/test/vts-testcase/kernel
849edb4
Zhuoyao Zhang
Zhuoyao Zhang

Fix Qtaguid sock parsing.
platform/test/vts-testcase/kernel
bff723e
Jerry Zhang
Jerry Zhang

remove ltp stable json config file from xml config
platform/test/vts-testcase/kernel
0cb5ecd
Yuexi Ma
Keun Soo Yim

Narrow down caller regex in VmallocInfo
platform/test/vts-testcase/kernel
a7967f5
Jerry Zhang
Jerry Zhang

Remove CONFIG_USB_G_ANDROID from base.
platform/test/vts-testcase/kernel
2575e13
Jerry Zhang
Jerry Zhang

Remove Features and Architecture from cpuinfo test.
platform/test/vts-testcase/kernel
80dad9b
Jerry Zhang
Jerry Zhang

remove ltp stable json config file from xml config
platform/test/vts-testcase/kernel
163ce93
Yuexi Ma
Yuexi Ma

Narrow down caller regex in VmallocInfo
platform/test/vts-testcase/kernel
31a013e
Jerry Zhang
Jerry Zhang

Remove CONFIG_USB_G_ANDROID from base.
platform/test/vts-testcase/kernel
1a49ba9
Jerry Zhang
Jerry Zhang

remove quota-related kernel configs
platform/test/vts-testcase/kernel
092cade
Steve Muckle
Steve Muckle

OWNERS: add additional owners for kernel configs
platform/test/vts-testcase/kernel
8f23f81
Steve Muckle
Steve Muckle

Remove Features and Architecture from cpuinfo test.
platform/test/vts-testcase/kernel
73f5a37
Jerry Zhang
Jerry Zhang

disable mm.thp01_32bit
platform/test/vts-testcase/kernel
20a7bcf
Steve Muckle
Steve Muckle

require LTP timer test suite to run in single thread
platform/test/vts-testcase/kernel
f673e71
Yuexi Ma
Yuexi Ma

LTP runner checks low ram property to choose scenario groups
platform/test/vts-testcase/kernel
a8f8d2d
Yuexi Ma
Yuexi Ma

LTP include filter should overwrite staging and disabled rules
platform/test/vts-testcase/kernel
8661e2c
Yuexi Ma
Yuexi Ma

require LTP timer test suite to run in single thread
platform/test/vts-testcase/kernel
179f3aa
Yuexi Ma
Yuexi Ma

LTP runner checks low ram property to choose scenario groups
platform/test/vts-testcase/kernel
4e5056e
Yuexi Ma
Yuexi Ma

LTP include filter should overwrite staging and disabled rules
platform/test/vts-testcase/kernel
88a6be2
Yuexi Ma
Yuexi Ma


