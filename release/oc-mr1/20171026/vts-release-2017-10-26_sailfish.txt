Release Date: 2017/10/26
Device build ID: 4417144 (branch: oc-mr1-release)
VTS build ID: 4417766 (branch: oc-mr1-dev)
AOSP system ID: 4417144 (branch: oc-mr1-release)

Total test modules: 150 of 150
Total passed test cases: 6718
Total failed test cases: 58
Total test execution time: 4h 30m 24s
Release decision: Distribute

Release notes:
Failing tests:

VtsHalOemHookV1_0Host
b/34344851 (known issue)

VtsHalWifiV1_0Host
b/68303248

VtsKernelLtp
b/68117785 (known issue)

VtsTreblePlatformVersionTest
b/68296627

Fixed issues:

VtsHalDumpstateV1_0Target
b/66328355 (known issue)

VtsHalSensorsV1_0Target
b/67439052

VTS changes contained:
