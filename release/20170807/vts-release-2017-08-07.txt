Release Date: 2017/08/07
Device build ID: 4243705 (branch: oc-release) 
VTS build ID: 4250459 (branch: oc-vts-release)
AOSP system build ID: 4249196 (branch: oc-treble-dev)

Total test modules: 96 of 98 
Total passed test cases: 4472
Total failed test cases: 3
Total test execution time: 3h 38m 56s
Release decision: ABANDON
Release notes:

Failing tests:
VtsHalGraphicsComposerV2_1Target(2 failing test cases)
known issue (b/64138694)

VtsHalMediaOmxV1_0Host(1 failing test cases)
Known flaky test, fixed on master (b/62602083)

VTS changes contained: 17

Subject
Project
SHA
Author
Committer

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
c901276
Ruchi Kandoi
Ruchi Kandoi

nfc: NCI 2.0: Add CORE_INIT_CMD for NCI 2.0
platform/hardware/interfaces
029633d
Ruchi Kandoi
Ruchi Kandoi

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
8aac1fc
Zhuoyao Zhang
Zhuoyao Zhang

Fix a few host-side test failure due to incorrect service name.
platform/test/vts-testcase/hal
43ac449
Zhuoyao Zhang
Zhuoyao Zhang

DO NOT MERGE: Revert "Add VNDK version check test case"
platform/test/vts-testcase/hal
cd54cff
Jiyong Park
Jiyong Park

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
15192a9
Badhri Jagan Sridharan
Keun Soo Yim

Exception for UsbHidlTest.switchModetoDFP
platform/test/vts-testcase/hal
aef4858
Badhri Jagan Sridharan
Keun Soo Yim

Remove Features and Architecture from cpuinfo test.
platform/test/vts-testcase/kernel
80dad9b
Jerry Zhang
Jerry Zhang

LTP number of threads configurable from runner
platform/test/vts-testcase/kernel
ab26517
Yuexi Ma
Yuexi Ma

remove ltp stable json config file from xml config
platform/test/vts-testcase/kernel
0cb5ecd
Yuexi Ma
Keun Soo Yim

Narrow down caller regex in VmallocInfo
platform/test/vts-testcase/kernel
a7967f5
Jerry Zhang
Jerry Zhang

Remove CONFIG_USB_G_ANDROID from base.
platform/test/vts-testcase/kernel
2575e13
Jerry Zhang
Jerry Zhang

DO NOT MERGE run everything in single thread mode
platform/test/vts-testcase/kernel
10432ad
Yuexi Ma
Yuexi Ma

remove ltp stable json config file from xml config
platform/test/vts-testcase/kernel
163ce93
Yuexi Ma
Yuexi Ma

Narrow down caller regex in VmallocInfo
platform/test/vts-testcase/kernel
31a013e
Jerry Zhang
Jerry Zhang

Remove CONFIG_USB_G_ANDROID from base.
platform/test/vts-testcase/kernel
1a49ba9
Jerry Zhang
Jerry Zhang

Remove Features and Architecture from cpuinfo test.
platform/test/vts-testcase/kernel
73f5a37
Jerry Zhang
Jerry Zhang
