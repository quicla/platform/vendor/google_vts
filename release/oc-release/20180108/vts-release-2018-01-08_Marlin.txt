Release Date: 2018/01/08
Device build ID: 4476836 (branch: goog/oc-release)
AOSP system build ID: 4531248 (branch: goog/oc-treble-dev)
VTS build ID: 4531415 (branch: aosp/oreo-vts-release)

Total test modules: 98 of 98
Total passed test cases: 4234
Total failed test cases: 4
Total test execution time: 4h 10m 37s
Release decision: Abandon

Release notes:

Features:
- Report: Pass
- -m Option: Pass (b/71789369_Option works, but Inaccurate runtime:for armeabi-v7a VtsKernelLtp, expected 1h 0s was 3h 17s)
- -m -t Option: Pass
- Sharding: Pass
- Retry (Plan): Pass
- Retry (Module): Fail
- Scripts: Pass

Failing tests:

VtsHalMediaOmxV1_0Host
b/71701712

VtsKernelLtp
b/71701585

Fixed issues:

VTS changes contained
