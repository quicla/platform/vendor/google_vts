Release Date: 2017/10/13
Device build ID: 4391570 (branch: oc-mr1-release)
VTS build ID: 4391570 (branch: oc-mr1-release)
AOSP system ID: 4391570 (branch: oc-mr1-release)
Build Tag: OPM1.171012.001

Total test modules: 150 of 150
Total passed test cases: 7065
Total failed test cases: 304
Total test execution time: 2h 14m 7s
Release decision: DISTRIBUTE
Release notes:

Failing tests:

VtsHalDumpstateV1_0Target
b/66328355 (known issue)

VtsHalNeuralnetworksV1_0Target
b/67771917

VtsHalOemHookV1_0Host
b/34344851 (known issue)

VtsHalOemLockV1_0Target
b/65270046 (known issue)

VtsHalRadioV1_0Target
b/67764964

VtsHalTetherOffloadControlV1_0Target
b/67439856 (known issue)

VtsKernelLtp
b/66336091 (known issue)

