#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

def EmitConsoleCommands(manifest_branch="git_oc-mr1-release",
                        build_target="aosp_walleye-userdebug",
                        build_id="latest",
                        test_name="vts",
                        shards=1,
                        serials=None,
                        **kwargs):
    """Runs a common VTS-on-GSI or CTS-on-GSI test.

    This uses a given device branch information and automatically selects a GSI branch
    and a test branch.

    Sample kwargs {u'build_id': u'4518263',
                   u'status': u'LEASED',
                   u'hostname': u'keun.mtv.corp.google.com',
                   u'shards': u'2',
                   u'param': [u''],
                   u'period': u'360',
                   u'priority': u'high',
                   u'manifest_branch': u'git_oc-mr1-release',
                   u'test_name': u'vts/vts-vndk',
                   u'device': u'vtslab-mtv43-main/walleye',
                   u'serial': [u'HT7BF1A02527', u'HT7BF1A01748'],
                   u'build_target': [u'aosp_walleye-userdebug']}
    """
    result = []

    if isinstance(build_target, list):
        build_target = build_target[0]

    result.append(
        "fetch --type=pab --branch=%s --target=%s --artifact_name=%s-img-%s.zip "
        "--build_id=%s" % (
            manifest_branch, build_target, build_target.split("-")[0],
            build_id if build_id != "latest" else "{build_id}", build_id),
    )

    if manifest_branch == "git_master":
      gsi_branch = "git_pi-release"
      test_branch = "git_pi-release"
      target = "test_suites_arm64"
    elif manifest_branch == "git_pi-release":
      gsi_branch = "git_pi-release"
      test_branch = "git_pi-release"
      target = "test_suites_arm64"
    elif manifest_branch == "git_oc-mr1-release":
      gsi_branch = "git_oc-mr1-treble-dev"
      test_branch = "git_oc-mr1-dev"
      target = "test_suites_arm64_fastbuild3l_linux"
    elif manifest_branch == "git_oc-release":
      gsi_branch = "git_oc-treble-dev"
      test_branch = "git_oc-vts-dev"
      target = "vts_arm_64"
    else:
      gsi_branch = manifest_branch
      test_branch = branch
      # TODO: derive target dynamically as an arg (uploaded to serving service.)
      target = "test_suites_arm64"

    result.append(
        "fetch --type=pab --branch=%s --target=aosp_arm64_ab-userdebug "
        "--artifact_name=aosp_arm64_ab-img-{build_id}.zip --build_id=latest" % gsi_branch,
    )
    result.append(
        "fetch --type=pab --branch=%s --target=%s --artifact_name=android-vts.zip "
        "--build_id=latest" % (test_branch, target),
    )
    if not serials:
        result.append(
            "flash --current"
        )
    else:
        sub_commands = []
        for serial in serials:
            sub_commands.append(
                "flash --current --serial %s" % serial
            )
        result.append(sub_commands)

    shards = int(shards)
    if shards > 1:
        sub_commands = []
        if shards <= len(serials):
            for shard_index in range(shards):
                sub_commands.append(
                    "test %s --shard-count=%d --shard-index=%d --serial %s" % (test_name, shards, shard_index, serials[shard_index]))
        result.append(sub_commands)
    else:
        if serials:
            result.append(
                "test %s --shards %s --serial %s" % (test_name, shards, ",".join(serials)))
        else:
            result.append(
                "test %s --shards %s" % (test_name, shards))

    print result
    return result
