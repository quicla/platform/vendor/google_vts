#!/bin/bash
# hal-interfaces android.hardware
# hal-interfaces android.hardware hardware/interfaces
function hal-interfaces() {
    [[ -n $2 ]] && pushd $2 > /dev/null
    find . -name "*.hal" \
        | sed "s/^\./$1/g" \
        | sed "s/\//\./g" \
        | sed "s/\.\([0-9]\+\.[0-9]\+\)\.\([a-zA-Z0-9]\+\)\.hal$/@\1::\2/g" \
        | sort | uniq
    [[ -n $2 ]] && popd > /dev/null
}

# hal-interfaces hardware/interfaces
function current-interfaces() {
    sed -e "s/\s*#.*$//g" ${1%%/}${1:+/}"current.txt" \
        | awk "{ print \$2 }" \
        | sed "/^ *$/d" \
        | sort | uniq
}

function hal-changes() {
    root=$1
    dir=$2
    if [[ -z $dir ]]; then
        dir=`pwd`
    fi

    echo "$1 ($2):"
    diff <(hal-interfaces $root $dir) <(current-interfaces $dir)
}

function hal-changes-report() {
    hal-changes android.hardware hardware/interfaces | grep -v "android.hardware.tests"
    hal-changes android.frameworks frameworks/hardware/interfaces
    hal-changes android.system system/hardware/interfaces
    hal-changes android.hidl system/libhidl/transport
}
