# coding=utf-8
# Copyright 2017 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Configuration for androidtestcenter[-test].appspot.com for jointbranch gce workers.
FOR USE ON PUBLIC SERVER ONLY.
See http://go/swarming for more details.
"""
from api import os_utilities
# pylint: disable=unused-argument
def get_state(bot):
  """Returns the bot's current state.
  We set that bots reboot every 12 hours.
  """
  state = os_utilities.get_state()
  state[u'periodic_reboot_secs'] = 12*60*60
  return check_quarantine(bot, state)
def on_after_task(bot, failure, internal_failure, *args):
  cleanup_dirs()
  check_internal_failure(internal_failure)
# TODO(http://crbug.com/702721) make this skia_common once that is supported
# Note, if changes are made to the below code, mirror them in skia_android.py
import errno
import getpass
import os
import shutil
import subprocess
import sys
import time
from api import platforms
_home = os.path.expanduser('~')
_NETRC_PATH = os.path.join(_home, '.netrc')
_BUILD_DEAD_PATH = os.path.join('/', 'b', 'build.dead')
# Windows uses C:/ for their configs
if sys.platform == 'win32':
  _home = "c:/"
  _BUILD_DEAD_PATH = os.path.join(_home, 'b', 'build.dead')
  # Windows also uses _netrc instead of .netrc for unknown reasons.
  _NETRC_PATH = os.path.join(_home, '_netrc')
_BOTO_PATH = os.path.join(_home, '.boto')
_GITCONFIG_PATH = os.path.join(_home, '.gitconfig')
_DIED_WARNING_PATH = os.path.join(_home, 'bot_died_warning')
_DIED_QUARANTINED_PATH = os.path.join(_home, 'bot_died_quarantined')
_DIRS_TO_CLEANUP = [_BUILD_DEAD_PATH]
def check_quarantine(bot, state):
  user = getpass.getuser()
  if user not in ('chrome-bot', 'swarming') and not state.get(u'quarantined'):
    state[u'quarantined'] = 'Bot denied running for user "%s"' % user
  # Non GCE bots should have .boto.
  if not platforms.is_gce():
    if not os.path.isfile(_BOTO_PATH):
      state[u'quarantined'] = u'.boto file is missing: %s' % _BOTO_PATH
  # All bots should have .gitconfig and .netrc
  if not os.path.isfile(_GITCONFIG_PATH):
    state[u'quarantined'] = u'.gitconfig file is missing: %s' % _GITCONFIG_PATH
  if not os.path.isfile(_NETRC_PATH):
    state[u'quarantined'] = u'.netrc file is missing: %s' % _NETRC_PATH
  # If bot has died back to back, it will touch _DIED_QUARANTINED_PATH to
  # signal it should be quarantined.
  if os.path.isfile(_DIED_QUARANTINED_PATH):
    state[u'quarantined'] = ('Internal Failure happened back to back. ' +
                             'Delete %s when fixed' % _DIED_QUARANTINED_PATH)
  return state
def check_internal_failure(internal_failure):
  if internal_failure:
    # If we experienced an internal_failure, create a warning file
    # If the next task also fails with internal_failure, quarantine
    # the bot.
    if os.path.exists(_DIED_WARNING_PATH):
      # Create the quarantined path
      open(_DIED_QUARANTINED_PATH, 'a').close()
    else:
      # Create the quarantined path
      open(_DIED_WARNING_PATH, 'a').close()
  else:
    # We passed, remove the warning if it was there.
    try:
      os.remove(_DIED_WARNING_PATH)
    except OSError:
      pass
# See skbug.com/6370 for context.
def cleanup_dirs():
  for d in _DIRS_TO_CLEANUP:
    RemoveDirectory(d)
# Copied the below block from chromium_utils.py:
# https://cs.chromium.org/chromium/build/scripts/common/chromium_utils.py?l=487
def RemoveDirectory(*path):
  """Recursively removes a directory, even if it's marked read-only.
  Remove the directory located at *path, if it exists.
  shutil.rmtree() doesn't work on Windows if any of the files or directories
  are read-only, which svn repositories and some .svn files are.  We need to
  be able to force the files to be writable (i.e., deletable) as we traverse
  the tree.
  Even with all this, Windows still sometimes fails to delete a file, citing
  a permission error (maybe something to do with antivirus scans or disk
  indexing).  The best suggestion any of the user forums had was to wait a
  bit and try again, so we do that too.  It's hand-waving, but sometimes it
  works. :/
  """
  file_path = os.path.join(*path)
  if not os.path.exists(file_path):
    return
  if sys.platform == 'win32':
    # Give up and use cmd.exe's rd command.
    file_path = os.path.normcase(file_path)
    for _ in xrange(3):
      print 'RemoveDirectory running %s' % (' '.join(
          ['cmd.exe', '/c', 'rd', '/q', '/s', file_path]))
      if not subprocess.call(['cmd.exe', '/c', 'rd', '/q', '/s', file_path]):
        break
      print '  Failed'
      time.sleep(3)
    return
  def RemoveWithRetry_non_win(rmfunc, path):
    if os.path.islink(path):
      return os.remove(path)
    else:
      return rmfunc(path)
  remove_with_retry = RemoveWithRetry_non_win
  def RmTreeOnError(function, path, excinfo):
    r"""This works around a problem whereby python 2.x on Windows has no ability
    to check for symbolic links.  os.path.islink always returns False.  But
    shutil.rmtree will fail if invoked on a symbolic link whose target was
    deleted before the link.  E.g., reproduce like this:
    > mkdir test
    > mkdir test\1
    > mklink /D test\current test\1
    > python -c "import chromium_utils; chromium_utils.RemoveDirectory('test')"
    To avoid this issue, we pass this error-handling function to rmtree.  If
    we see the exact sort of failure, we ignore it.  All other failures we re-
    raise.
    """
    exception_type = excinfo[0]
    exception_value = excinfo[1]
    # If shutil.rmtree encounters a symbolic link on Windows, os.listdir will
    # fail with a WindowsError exception with an ENOENT errno (i.e., file not
    # found).  We'll ignore that error.  Note that WindowsError is not defined
    # for non-Windows platforms, so we use OSError (of which it is a subclass)
    # to avoid lint complaints about an undefined global on non-Windows
    # platforms.
    if (function is os.listdir) and issubclass(exception_type, OSError):
      if exception_value.errno == errno.ENOENT:
        # File does not exist, and we're trying to delete, so we can ignore the
        # failure.
        print 'WARNING:  Failed to list %s during rmtree.  Ignoring.\n' % path
      else:
        raise
    else:
      raise
  for root, dirs, files in os.walk(file_path, topdown=False):
    # For POSIX:  making the directory writable guarantees removability.
    # Windows will ignore the non-read-only bits in the chmod value.
    os.chmod(root, 0770)
    for name in files:
      remove_with_retry(os.remove, os.path.join(root, name))
    for name in dirs:
      remove_with_retry(lambda p: shutil.rmtree(p, onerror=RmTreeOnError),
                        os.path.join(root, name))
  remove_with_retry(os.rmdir, file_path)