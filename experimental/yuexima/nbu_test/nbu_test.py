#
# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from vts.runners.host import base_test


class NbuBaseTest(base_test.BaseTestClass):
    '''Base template class for NBU test cases.'''

    def setUpClass(self):
        """Setup function that will be called before executing any test case in
          the test class.

          To signal setup failure, return False or raise an exception. If
          exceptions were raised, the stack trace would appear in log, but the
          exceptions would not propagate to upper levels.

          Implementation is optional.
        """
        if callable(getattr(self, "setup_class", None)):
            self.setup_class()

    def setUp(self):
        """Setup function that will be called every time before executing each
        test case in the test class.

        To signal setup failure, return False or raise an exception. If
        exceptions were raised, the stack trace would appear in log, but the
        exceptions would not propagate to upper levels.

        Implementation is optional.
        """
        if callable(getattr(self, "setup_test", None)):
            self.setup_test()

    def onFail(self, test_name, begin_time):
        """A function that is executed upon a test case failure.

        User implementation is optional.

        Args:
            test_name: Name of the test that triggered this function.
            begin_time: Logline format timestamp taken when the test started.
        """
        if callable(getattr(self, "on_fail", None)):
            self.on_fail()

    def tearDown(self):
        """Teardown function that will be called every time a test case has
        been executed.

        Implementation is optional.
        """
        if callable(getattr(self, "teardown_test", None)):
            self.teardown_test()

    def tearDownClass(self):
        """Teardown function that will be called after all the selected test
        cases in the test class have been executed.

        Implementation is optional.
        """
        pass
        if callable(getattr(self, "teardown_class", None)):
            self.teardown_class()