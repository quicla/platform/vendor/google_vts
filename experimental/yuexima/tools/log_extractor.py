# Tool for extracting useful logs from a large log file, e.g. release log file.
# Useage: specify keywords, file_path, and retain number x, then
#         all x lines following any of the words in the log will be extracted
#         to a new file with suffix _filtered.txt

import os

keywords = ['']

file_path = os.path.expanduser('~') + '/' + ''

retain_pre = 100
retain_post = 100
case_sensitive = False
to_print = True

keywords = filter(bool, keywords)
fp_new = file_path + '_filtered.txt'

if not case_sensitive:
    keywords = [w.lower() for w in keywords]

with open(fp_new, 'w') as fn:
    c = retain_post
    buffer = []

    for line in open(file_path, 'r'):
        buffer.append(line)
        if len(buffer) > retain_pre:
            del buffer[0]

        if not case_sensitive:
            line = line.lower()
        for w in keywords:
            if w in line:
                c = 0
                break

        if c < retain_post:
            while buffer:
                l = buffer.pop(0)
                if to_print:
                    print(l[:-1])
                fn.write(l)
        elif c == retain_post:
            fn.write(
                '\n\n\n-------------------------------- ... --------------------------------\n\n\n'
            )

        c += 1

print('finished')
