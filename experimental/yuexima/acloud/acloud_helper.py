#
# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
from os.path import expanduser

from acloud.public import acloud_main
from vts.utils.python.common import cmd_utils

DEFAULT_BRANCH = 'git_oc-mr1-release'
DEFAULT_BUILD_TARGET = 'gce_x86_phone-userdebug'

class ACloudHelper(object):
    '''Helper class to manage access to the acloud module.'''

    def GetCreateCmd(self, build_id):
        '''Get acould create command with given build id.

        Args:
            build_id: string, build_id.

        Returns:
            string, acloud create command.
        '''
        #TODO latest build id.
        #TODO use unique log and tmp file location
        cmd = ('create '
            '--branch {branch} '
            '--build_target {build_target} '
            '--build_id {build_id} '
            '--config_file {config_file} '
            '--report_file {report_file} '
            '--log_file {log_file} '
            '--email {email}').format(
          branch = DEFAULT_BRANCH,
          build_target = DEFAULT_BUILD_TARGET,
          build_id = build_id,
          config_file = expanduser("~") + '/acloud.config',
          report_file = '/tmp/acloud_report.json',
          log_file = '/tmp/acloud.log',
          email = 'android-vts@google.com'
          )
        return cmd

    def CreateInstance(self, build_id):
        cmd = self.GetCreateCmd(build_id)
        acloud_main.main(cmd.split())

    def PrepareConfig(self):
        cmd = ('cat /google/src/head/depot/google3/cloud/android/driver/'
               'public/data/users/command_line_sample.config > '
               '{directory}/acloud.config').format(directory=expanduser("~"))

        results = cmd_utils.ExecuteShellCommand(cmd)
        if any(results[cmd_utils.EXIT_CODE]):
            logging.error("Fail to execute command: %s" % cmd)

    def GetInstanceIP(self):
        report_file = '/tmp/acloud_report.json'

        for line in open(report_file, 'r'):
            if '"status"' in line and 'SUCCESS' not in line:
                logging.error('Failed to create acloud instance.')
                #TODO check log
                return None

            if '"ip":' in line:
                ip = line.replace('"ip": "', '').replace('",', '').strip()
                print(ip)
                logging.info('acloud instance ip: %s' % ip)
                return ip
        logging.error('Failed to parse acloud create command result.')
        #TODO provide more failing info if reached to end

    def ConnectInstanceToAdb(self, ip=None):
        if not ip:
            ip = self.GetInstanceIP()

        cmds = [('ssh -i ~/.ssh/acloud_rsa -o UserKnownHostsFile=/dev/null '
                 '-o StrictHostKeyChecking=no -L 40000:127.0.0.1:6444 -L '
                 '30000:127.0.0.1:5555 -N -f -l root %s') % ip,
                 'adb connect 127.0.0.1:30000']

        for cmd in cmds:
            results = cmd_utils.ExecuteShellCommand(cmd)
            if any(results[cmd_utils.EXIT_CODE]):
                logging.error("Fail to execute command: %s\nResult:" % (cmd,
                                                                        results))
                return
